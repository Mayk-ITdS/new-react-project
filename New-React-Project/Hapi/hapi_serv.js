const Hapi = require("@hapi/hapi");
const sqlite3 = require("sqlite3");
const db = new sqlite3.Database("cars.db");
const Path = require("path");
const inert = require("@hapi/inert");
const Joi = require("@hapi/joi");

const init = async () => {
  const server = Hapi.server({
    port: 3000,
    host: "localhost",
    routes: {
      cors: true,
      files: {
        relativeTo: Path.join(__dirname, "../react-crud"),
      },
    },
  });

  await server.register(inert);

  server.route({
    method: "GET",
    path: "/{param*}",
    handler: {
      directory: {
        path: ".",
        redirectToSlash: true,
        index: true,
      },
    },
  });
  server.route({
    method: "GET",
    path: "/cars",
    options: {
      validate: {
        query: Joi.object({
          limit: Joi.number().integer().min(1).max(100).default(10),
          name: Joi.string().min(1).max(50).optional(),
          year: Joi.number()
            .integer()
            .min(1900)
            .max(new Date().getFullYear())
            .optional(),
        }),
      },
    },
    handler: async (request, h) => {
      try {
        const { limit, name, year } = request.query;
        const conditions = [];
        const parameters = [];

        if (name) {
          conditions.push(`name LIKE ?`);
          parameters.push(`%${name}%`);
        }

        if (year) {
          conditions.push(`year = ?`);
          parameters.push(year);
        }

        let sql = `SELECT cars.*, parts.* from cars left join parts on Cars.ID = Parts.Cars_ID`;
        if (conditions.length) {
          sql += ` WHERE ` + conditions.join(" AND ");
        }
        sql += ` ORDER BY id DESC LIMIT ?`;
        parameters.push(limit);

        const query = await new Promise((resolve, reject) => {
          db.all(sql, parameters, (error, rows) => {
            if (error) {
              reject(error);
            } else {
              resolve(rows);
            }
          });
        });

        if (query.length === 0) {
          return h.response({ message: "No cars found" }).code(404);
        }
        return h.response(query).code(200);
      } catch (error) {
        console.error(error);
        return h.response({ error: "Internal Server Error" }).code(500);
      }
    },
  });

  server.route({
    method: "POST",
    path: "/cars",
    options: {
      validate: {
        payload: Joi.object({
          name: Joi.string().min(1).max(20).required(),
          mark: Joi.string().min(1).max(20).required(),
          age: Joi.number().integer().required(),
          country: Joi.string().min(1).max(20).required(),
        }),
      },
    },

    handler: async (request, h) => {
      const { name, mark, age, country, type } = request.payload;
      const age_int = parseInt(age);
      var price = 0;
      if (age >= 5 && type == "Engine") {
        price = 25000;
      }
      if (age < 5 || type == "Wheel" || type == "Gearbox") {
        price = 30000;
      }

      // Start a database transaction
      try {
        const carId = await new Promise((resolve, reject) => {
          db.run(`BEGIN TRANSACTION;`, (error) => {
            if (error) reject(error);
          });
          db.run(
            `INSERT INTO Cars(Name, Mark, Age, Country) VALUES (?, ?, ?, ?);`,
            [name, mark, age_int, country],
            function (error) {
              if (error) {
                db.run(`ROLLBACK;`, () => reject(error));
              } else {
                resolve(this.lastID); // this.lastID contains the last inserted row ID
              }
            }
          );
        });

        await new Promise((resolve, reject) => {
          db.run(
            `INSERT INTO Parts(Price_$, Cars_ID) VALUES (?, ?);`,
            [price, carId],
            (error) => {
              if (error) {
                db.run(`ROLLBACK;`, () => reject(error));
              } else {
                db.run(`COMMIT;`, () => resolve());
              }
            }
          );
        });

        return h
          .response({ id: carId, message: "Car added successfully" })
          .code(201);
      } catch (error) {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx and the error details
          console.error("Error data:", error.response.data);
          console.error("Error status:", error.response.status);
        } else if (error.request) {
          // The request was made but no response was received
          console.error("Error request:", error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.error("Error message:", error.message);
        }
      }
    },
  });

  server.route({
    method: "PUT",
    path: "/cars/{id}",
    handler: async (request, h) => {
      const { id } = request.params;
      const { name, mark, age, country } = request.payload;
      const sql = `UPDATE Cars SET Name = ?, Mark = ?, Age = ?, Country = ? WHERE ID = ?`;
      try {
        const result = await new Promise((resolve, reject) => {
          db.run(sql, [name, mark, age, country, id], function (error) {
            if (error) {
              reject(error);
            } else {
              resolve(this.changes);
            }
          });
        });
        if (result === 0) {
          return h.response({ message: "Car not found" }).code(404);
        }
        return h.response({ message: "Car updated successfully" }).code(200);
      } catch (error) {
        console.error(error);
        return h.response({ error: "Internal Server Error" }).code(500);
      }
    },
  });

  server.route({
    method: "DELETE",
    path: "/cars/{id}",
    options: {
      validate: {
        params: Joi.object({
          id: Joi.number().integer().positive().required(),
        }),
      },
    },
    handler: async (request, h) => {
      const { id } = request.params;
      const sql = `DELETE FROM Cars WHERE ID = ?`;
      try {
        const result = await new Promise((resolve, reject) => {
          db.run(sql, [id], function (error) {
            if (error) {
              reject(error);
            } else {
              resolve(this.changes);
            }
          });
        });
        if (result === 0) {
          return h.response({ message: "Car not found" }).code(404);
        }
        return h.response({ message: "Car deleted successfully" }).code(204);
      } catch (error) {
        console.error(error);
        return h.response({ error: "Internal Server Error" }).code(500);
      }
    },
  });

  await server.start();
  console.log("Server running on %s", server.info.uri);
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();
