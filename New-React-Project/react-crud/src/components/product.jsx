import { Link } from "react-router-dom";

const Product = ({car}) => {
    let source = () => { 
    if (car.Name === 'Sedan'){
        return '/honda-sedan.jpg'
    } 
    else (car.Name === 'Cabrio')
    {
        return '/honda-cabrio.jpg'    
    }
        
    
     
        
     
        
};
    return (
        <div className="Product">
            <img src={source()} className="w-full h-28 object-cover"/>
            <div className="container_02">
                <div className="text-sm">ID: {car.ID}</div>
                <h2 className="text font-semibold">{car.Mark}</h2>
                <div className="text-sm">Type: {car.Name}</div>
                <div className="text-sm">Age: {car.Age} years</div>
                <div className="text-sm">Country: {car.Country}</div>
                <Link to={`/edit/${car.ID}`} className="edit-button">Edit</Link>
                <Link to={`/delete/${car.ID}`} className="delete-button">Delete</Link> 
            </div>
            <div className="ProductActions">
                
                
            
            </div>
        </div>
    )
}

export default Product;