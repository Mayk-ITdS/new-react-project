import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';

const EditPage = () => {
    const { id } = useParams();
    const navigate = useNavigate();
    const [car, setCar] = useState({
        name: '',
        mark: '',
        age: '',
        country: '',
    });
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');

    useEffect(() => {
        setLoading(true);
        axios.get(`http://localhost:3000/cars/${id}`)
            .then(response => {
                setCar(response.data);
                setLoading(false);
            })
            .catch(error => {
                setError('Could not fetch car details.');
                setLoading(false);
            });
    }, [id]);

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setCar(prevCar => ({
            ...prevCar,
            [name]: value
        }));
    };

    const handleSave = () => {
        setLoading(true);
        axios.put(`http://localhost:3000/cars/${id}`, car)
            .then(() => {
                setLoading(false);
                navigate('/');
            })
            .catch(() => {
                setError('Failed to save car.');
                setLoading(false);
            });
    };

    const handleDelete = () => {
        const confirmDelete = window.confirm('Are you sure you want to delete this car?');
        if (confirmDelete) {
            setLoading(true);
            axios.delete(`http://localhost:3000/cars/${id}`)
                .then(() => {
                    setLoading(false);
                    navigate('/');
                })
                .catch(() => {
                    setError('Failed to delete car.');
                    setLoading(false);
                });
        }
    };

    if (loading) return <p>Loading...</p>;
    if (error) return <p>{error}</p>;

    return (
        <div className="edit-container">
            <h2>Edit Car</h2>
            
            <input
                name="name"
                value={car.name}
                onChange={handleInputChange}
            />
            <input
                name="mark"
                value={car.mark}
                onChange={handleInputChange}
            />
            <input
                name="age"
                type="number"
                value={car.age}
                onChange={handleInputChange}
            />
            <input
                name="country"
                value={car.country}
                onChange={handleInputChange}
            />
            <button onClick={handleSave}>Save Changes</button>
            <button onClick={handleDelete}>Delete Car</button>
        </div>
    );
};

export default EditPage;