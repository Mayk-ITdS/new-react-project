import 'semantic-ui-css/semantic.min.css';
import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { Form, Input, Button, Dropdown, Grid, Segment, Header } from 'semantic-ui-react';
 

const countryOptions = [
  { key: 'jpn', text: 'Japan', value: 'Japan' },
  { key: 'col', text: 'Columbia', value: 'Columbia' },
  { key: 'pln', text: 'Poland', value: 'Poland' },
  { key: '', text: 'USA', value: 'USA' },
 
];

const CreatePage = () => {
  const [name, setName] = useState('');
  const [mark, setMark] = useState('');
  const [age, setAge] = useState('');
  const [country, setCountry] = useState('');
  const navigate = useNavigate();

  const saveCar = async () => {
    if (!name || !mark || !age || !country) {
      toast.error('All fields must be filled out');
      return;
    }
    
    
      if (isNaN(age) || age <= 0) {
        toast.error('Please enter a valid age');
        return;
      }
       
      if (age > 100) {
        toast.error('Please enter a realistic age');
        return;
      }
    
      if (name.length < 2 || name.length > 50) {
        toast.error('Name must be between 2 and 50 characters');
        return;
      }
     
    try {
      const payload = { name, mark, age, country };
      await axios.post('http://localhost:3000/cars', payload);
      toast.success(`Car successfully saved`);
      setTimeout(() => navigate('/'), 3000);
    } catch (error) {
      console.error('Error creating car:', error);
      toast.error('Failed to create car');
    }
  };

  return (
    <Grid centered columns={3}>
      <Grid.Column>
        <Segment padded="very">
          <Header as='h2' textAlign='center'>
            Create a Car
          </Header>
          <Form onSubmit={saveCar}>
            <Form.Field
              control={Input}
              label='Name'
              placeholder='Name'
              value={name}
              onChange={(e,{ value }) => setName(value)}
              required
            />
            <Form.Field
              control={Input}
              label='Mark'
              placeholder='Mark'
              value={mark}
              onChange={(e,{ value }) => setMark(value)}
              required
            />
            <Form.Field
              className='no-spinners'
              control={Input}
              label='Age'
              placeholder='Age'
              value={age}
              onChange={(e,{ value }) => setAge(value)}
              required
            />
            <Form.Field
              control={Dropdown}
              label='Country'
              placeholder='Select Country'
              fluid
              selection
              options={countryOptions}
              onChange={(e,{ value }) => setCountry(value)}
              value={country}
              required
            />
            <Button type="submit" color="green" fluid>
              Save
            </Button>
          </Form>
        </Segment>
      </Grid.Column>
    </Grid>
  );
};

export default CreatePage;